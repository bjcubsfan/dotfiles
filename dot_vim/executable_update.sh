#!/usr/bin/env bash
#
# Usage: ./update.sh [pattern]
#
# Specify [pattern] to update only repos that match the pattern.

repos=(

tpope/vim-pathogen  # Plugin manager for vim
tpope/vim-flagship  # Status and tab line
arcticicestudio/nord-vim # Nord color scheme
dracula/vim # dracula color scheme
altercation/vim-colors-solarized # solarized color scheme
junegunn/fzf  # fuzzy finding binary (also has plugin)
junegunn/fzf.vim  # fuzzy finding
mileszs/ack.vim  # fast code search
preservim/vim-indent-guides  # Show indentation guides
Shougo/ddc.vim  # Good tab completion
Shougo/ddc-around  # matches around the cursor for ddc
Shougo/ddc-matcher_head  # matches around head
Shougo/ddc-sorter_rank  # ranks the matches
vim-denops/denops.vim  # ddc requirement
ervandew/supertab # Supertab to use tab
nathangrigg/vim-beancount  # syntax for beancount
valloric/matchtagalways  # highlight matching html tags
kchmck/vim-coffee-script  # coffee script support
christoomey/vim-tmux-navigator  # navigate windows simply
)

set -e
dir=~/.vim/bundle

if [ -d $dir -a -z "$1" ]; then
  temp="$(mktemp -d -t bundleXXXXX)"
  echo "▲ Moving old bundle dir to $temp"
  mv "$dir" "$temp"
fi

mkdir -p $dir

for repo in ${repos[@]}; do
  if [ -n "$1" ]; then
    if ! (echo "$repo" | grep -i "$1" &>/dev/null) ; then
      continue
    fi
  fi
  plugin="$(basename $repo | sed -e 's/\.git$//')"
  [ "$plugin" = "vim-styled-jsx" ] && plugin="000-vim-styled-jsx" # https://goo.gl/tJVPja
  [ "$plugin" = "vim" ] && plugin="dracula" # rename dracula
  dest="$dir/$plugin"
  rm -rf $dest
  (
    git clone --depth=1 -q https://github.com/$repo $dest
    rm -rf $dest/.git
    echo "· Cloned $repo"
  ) &
done
wait


My configuration files for both Mac OS and Linux machines. I manage and apply configurations using [chezmoi](https://www.chezmoi.io/).

## Things to install for the shell:

Note that on Linux, paste sometimes is `CTRL-Shift-V` for the terminal.

- [oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh)

```
git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh
```

- [zsh-vi-mode](https://github.com/jeffreytse/zsh-vi-mode)

```
git clone https://github.com/jeffreytse/zsh-vi-mode $ZSH_CUSTOM/plugins/zsh-vi-mode
```

- [Powerlevel10k](https://github.com/romkatv/powerlevel10k)

```
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
```

## Updates to make on a new machine

Add all keys to the ssh-agent which can be done with commands like:

```
ssh-add --apple-use-keychain ~/.ssh/ipa-id-ed25519
```

Clone the plugin manager for `tmux`:

```
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```

Then in `tmux` run `CTRL-a I` to download the plugins.

```
git clone https://github.com/zsh-users/zsh-history-substring-search ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-history-substring-search
```

## Things to fix

[ ] - no UseKeychain in ssh config for Linux
[ ] - different paths for python in vimrc depending on the machine:


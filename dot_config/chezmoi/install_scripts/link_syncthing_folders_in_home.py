#!/usr/bin/env python

import logging
import os

import click

# allow -h for help as well
CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])

def should_skip_item(item):
    to_ignore = [".DS_Store", ".stignore", ".stfolder"]
    if item.name in to_ignore or not item.is_dir():
        return True
    else:
        return False

@click.command(context_settings=CONTEXT_SETTINGS)
@click.option("-d", "--dry-run/--no-dry-run", default=False, help="Print only, do not do anything")
def main(dry_run):
    home = os.environ["HOME"]
    syncthing = os.path.join(home, "syncthing")
    os.chdir(home)
    for item in os.scandir(syncthing):
        if should_skip_item(item):
            continue
        for sub_item in os.scandir(item.path):
            if should_skip_item(sub_item):
                continue
            this_source = os.path.join("syncthing", item.name, sub_item.name)
            this_dest = sub_item.name
            if os.path.exists(os.path.join(home, this_dest)):
                logging.info(f'Already have link, skipping "{this_source}" to "{this_dest}".')
            else:
                logging.info(f'Will link "{this_source}" to "{this_dest}".')
                if not dry_run:
                    os.symlink(this_source, this_dest)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")
    main()

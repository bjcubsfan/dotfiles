#!/bin/sh

# Install Homebrew, if not already installed
which brew > /dev/null || ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

# Homebrew currently installed generated with:
# brew bundle dump
cd $HOME/.config/homebrew && brew bundle

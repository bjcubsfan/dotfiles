set-option -g history-limit 50000

set -g prefix C-a
unbind C-b
bind-key C-a send-prefix

unbind %
bind | split-window -h

unbind '"'
bind - split-window -v

unbind r
bind r source-file ~/.tmux.conf

bind -r j resize-pane -D 5
bind -r k resize-pane -U 5
bind -r l resize-pane -R 5
bind -r h resize-pane -L 5

bind -r m resize-pane -Z

set-window-option -g mode-keys vi

bind-key -T copy-mode-vi 'v' send -X begin-selction
bind-key -T copy-mode-vi 'y' send -X copy-selction

unbind -T copy-mode-vi MouseDragEnd1Pane

set -g mouse on

# Change indexes when a window is deleted
set -g renumber-windows on

# tpm plugin
set -g @plugin 'tmux-plugins/tpm'

# Window swap menu is C-A <

# list of tmux plugins
set -g @plugin 'christoomey/vim-tmux-navigator'
set -g @plugin 'jimeh/tmux-themepack'
set -g @plugin 'tmux-plugins/tmux-resurrect'  # persist tmux sessions after computer restart
set -g @plugin 'tmux-plugins/tmux-continuum'  # automatically saves sessions for you every 15 minutes

set -g @themepack 'powerline/double/cyan'

set -g @resurrect-capture-pane-contents 'on'
{{ if hasSuffix "waas.lab" .chezmoi.fqdnHostname }}
set -g @resurrect-dir '$HOME/.tmux-resurrect/$HOSTNAME/'
{{ end }}
set -g @continuum-restore 'on'

run '~/.tmux/plugins/tpm/tpm'

# 2024-03-29 nvim :checkhealth recommends
set-option -sg escape-time 10
set-option -g focus-events on
#set-option -g default-terminal "screen-256color"
set-option -sa terminal-features ',alacritty:RGB'
set -g default-terminal "tmux-256color"
set -ga terminal-overrides ",xterm-256color:Tc"
